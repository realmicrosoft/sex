use alloc::vec::Vec;
use crate::serial::*;

// for booting, we'll be using legacy stuff so we're only gonna care about the first two bussies
static PRIMARY_BUS: u16 = 0x1F0;
static SECONDARY_BUS: u16 = 0x170;

struct AtaDisk {
    pub bus: u16,
    pub m_or_s: bool,
    pub device_info: [u16; 256],
}

fn query_drive(bus: u16, m_or_s: bool) -> Option<AtaDisk> {
    let target_command = if m_or_s { 0xA0 } else { 0xB0 };
    let drive_select = bus + 6;
    let sector_count = bus + 2;
    let lba_lo = bus + 3;
    let lba_mid = bus + 4;
    let lba_hi = bus + 5;

    let identify_command = 0xEC;
    let command_port = bus + 7;

    // select the drive
    command_b(drive_select, target_command);

    // set sector_count through lba_hi to 0
    command_b(sector_count, 0);
    command_b(lba_lo, 0);
    command_b(lba_mid, 0);
    command_b(lba_hi, 0);

    // send the identify command
    command_b(command_port, identify_command);

    // wait for the drive to respond
    let status = read_b(command_port);
    if status == 0 {
        return None; // drive not found
    }
    while status & 0x80 == 0 {
        status = read_b(command_port);
    }
    // check lbamid and lbahi ports to see if they're non-zero
    let lbamid = read_b(lba_mid);
    let lbahi = read_b(lba_hi);
    if lbamid != 0 || lbahi != 0 {
        return None; // not correct drive
    }

    // await bit 3 or bit 0 of the status port to be set
    while status & 8 == 0 && status & 1 == 0 {
        status = read_b(command_port);
    }

    // read the drive info
    let mut drive_info = [0; 256];
    let data_port = bus + 0;
    for i in 0..256 {
        drive_info[i] = readW(data_port);
    }


}

// reads the status byte and checks if it is equal to 0xFF
// if so, this bus has no drives attached
fn check_bus(base: u16) -> bool {
    // status register is 7 offset from IO base
    let status_reg = base + 7;
    let status = readB(status_reg);
    status == 0xFF
}

pub fn query_all_drives() -> Vec<AtaDisk> {
    let mut drives = Vec::new();
    // check if primary bus has drives
    let bussies = [PRIMARY_BUS, SECONDARY_BUS];
    for bus in bussies.iter() {
        if !check_bus(*bus) {
            continue;
        }
        // check if bus has master or slave
        let m_or_s = *bus == PRIMARY_BUS;
        let drive = query_drive(*bus, m_or_s);
        if let Some(drive) = drive {
            drives.push(drive);
        }
    }
    drives
}