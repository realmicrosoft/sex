use uefi::proto::console::gop::BltPixel;
use crate::{font, SexInfo, SEXINFO};
use crate::font::*;

pub fn put_pixel(x: i32, y: i32, colour: BltPixel) {
    let mut buffer = SEXINFO.page_two.lock(); // buffer for drawing
    let width = SEXINFO.width.lock();
    let index = (y * width.clone() as i32 + x) as usize;
    buffer[index] = colour;
}

pub fn put_char(x: i32, y: i32, c: char, colour: BltPixel) {
    let font = font::BASIC_LEGACY;
    // font is 8x8, stored in a 2d array of bytes
    let char_width = 8;
    let char_height = 8;

    let char_index = c as usize;
    let char_data = font[char_index];

    for row in 0..char_height {
        for col in 0..char_width {
            let bit = (char_data[row] >> col) & 1;
            if bit >= 1 {
                put_pixel(x + col as i32, y + row as i32, colour);
            }
        }
    }
}

pub fn put_string(mut x: i32, y: i32, s: &str, colour: BltPixel) {
    for c in s.chars() {
        put_char(x, y, c, colour);
        x += 8;
    }
}

pub fn draw_box(x: i32, y: i32, width: i32, height: i32, colour: BltPixel) {
    let mut buffer = SEXINFO.page_two.lock(); // buffer for drawing
    let pp_scanline = SEXINFO.width.lock();

    for i in 0..width {
        for j in 0..height {
            let index = (y + j) * pp_scanline.clone() as i32 + x + i;
            buffer[index as usize] = colour;
        }
    }
}